package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	httpRouter "bitbucket.org/simonhartmann/router"
)

// The AdminServer is the access point for user to push new configs into the system which
// then gets distributed to the connected relay servers and clients.
type AdminServer struct {
	router *httpRouter.Router
	Files  *FileContainer
}

// StartAdminServer starts and runs a admin server on the given endpoint.
func StartAdminServer(endpoint string, container *FileContainer) *AdminServer {
	srv := &AdminServer{
		router: httpRouter.NewRouter(),
		Files:  container,
	}

	srv.registerAPI()

	go func() {
		err := http.ListenAndServe(endpoint, srv.router)
		if err != nil {
			log.Fatal(err)
		}
	}()
	fmt.Printf("Admin server running on '%s'\n", endpoint)

	return srv
}

func (srv *AdminServer) GetFileContent(filename string) ([]byte, error) {
	srv.Files.RLock()
	defer srv.Files.RUnlock()

	f, ok := srv.Files.Collection[filename]
	if !ok {
		return nil, errors.New("file not found")
	}
	return f.data, nil
}

// GetFileNames returns the file names known to the admin server
func (srv *AdminServer) GetFileNames() []string {
	srv.Files.RLock()
	defer srv.Files.RUnlock()

	keys := make([]string, 0, len(srv.Files.Collection))
	for k := range srv.Files.Collection {
		keys = append(keys, k)
	}
	return keys
}

// SetConfig creates or overrides the file with given name and content
func (srv *AdminServer) SetConfig(filename string, content []byte) []byte {
	srv.Files.Lock()
	defer srv.Files.Unlock()

	f, ok := srv.Files.Collection[filename]
	if !ok {
		f = FileInfo{
			data:       content,
			lastUpdate: time.Now().UTC(),
			name:       filename,
		}
		srv.Files.Collection[filename] = f
	}

	f.data = content

	// update subscribers

	return f.data
}

// Register the HTTP API
func (srv *AdminServer) registerAPI() {
	srv.router.RedirectSlash = true
	srv.router.Get("/config/{filename}/", http.HandlerFunc(srv.httpGetConfig))
	srv.router.Get("/configs/", http.HandlerFunc(srv.httpGetIndex))
	srv.router.Post("/config/{filename}/", http.HandlerFunc(srv.httpSetConfig))
}

// HTTP handlers
// ...
func (srv *AdminServer) httpGetConfig(w http.ResponseWriter, r *http.Request) {
	// TODO: wrap response into {"content":bla}
	filename := httpRouter.GetParam(r, "filename")
	content, err := srv.GetFileContent(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
	} else {
		fmt.Fprint(w, content)
	}
}

func (srv *AdminServer) httpGetIndex(w http.ResponseWriter, r *http.Request) {
	filenames := srv.GetFileNames()
	b, err := json.Marshal(filenames)
	if err != nil {
		http.Error(w, err.Error(), 500)
	} else {
		w.Write(b)
	}
}

func (srv *AdminServer) httpSetConfig(w http.ResponseWriter, r *http.Request) {
	// TODO: wrap response into {"content":bla}
	filename := httpRouter.GetParam(r, "filename")
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 400)
	} else {
		content := srv.SetConfig(filename, content)
		fmt.Fprint(w, content)
	}
}
