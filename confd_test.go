package main

import (
	"testing"

	"bitbucket.org/simonhartmann/assert"
)

func TestAddConfigSimple(t *testing.T) {
	container := CreateFileContainer()
	as := StartAdminServer("127.0.0.1:0", container)

	confname := "test.conf"
	confcontent := "{\"data\":\"dummy\"}"

	as.SetConfig(confname, []byte(confcontent))
	ret, err := as.GetFileContent(confname)

	assert.IsTrue(err == nil, t)
	assert.AreEqual(string(ret), confcontent, t)
}

func TestGetConfigRelay(t *testing.T) {
	container := CreateFileContainer()
	as := StartAdminServer("127.0.0.1:0", container)
	StartRelayServer("127.0.0.1:0", "", container)

	confname := "test.conf"
	confcontent := "{\"data\":\"dummy\"}"

	as.SetConfig(confname, []byte(confcontent))
	ret, err := as.GetFileContent(confname)

	assert.IsTrue(err == nil, t)
	assert.AreEqual(string(ret), confcontent, t)
}

func TestRegisterAtParent(t *testing.T) {
	container := CreateFileContainer()
	rs1 := StartRelayServer("127.0.0.1:0", "", container)
	addr := rs1.GetLocalEndpoint()
	StartRelayServer("127.0.0.1:0", addr, container)
}
