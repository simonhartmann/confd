package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	httpRouter "bitbucket.org/simonhartmann/router"
)

// Relay servers are forming the a grid between AdminServer and the clients. Relay server
// are also able to be clients (they share the interface), so it's possible to have
// a deeper hierarchy with one relay server connecting to three 'client' relay servers
// (e.g. on localhost) which will finally speak to the client
//
//              | admin |
//            /     |     \
//       |relay| |relay|  |relay|
//       /    \       \        \
//  |client| |client| |client| |client|
//

// RelayServer container
type RelayServer struct {
	router            *httpRouter.Router
	Files             *FileContainer
	SubscriptionsLock sync.RWMutex
	Subscriptions     map[string]Subscription
	parentEndpoint    string
	endpoint          string
	listener          net.Listener
	server            *http.Server
}

// Copied from go http source - seems like we really have to copy
// that code to get the port of an http server after it was initialized with :0
// I bet there is a smarter way...
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(3 * time.Minute)
	return tc, nil
}

// StartRelayServer starts and runs a relay server on given endpoint.
func StartRelayServer(endpoint string, parentEndpoint string, container *FileContainer) *RelayServer {
	srv := &RelayServer{
		router:         httpRouter.NewRouter(),
		Files:          container,
		Subscriptions:  make(map[string]Subscription),
		parentEndpoint: parentEndpoint,
		endpoint:       endpoint,
	}

	srv.server = &http.Server{Addr: srv.endpoint, Handler: srv.router}
	addr := srv.server.Addr
	if addr == "" {
		addr = ":http"
	}

	var err error

	srv.listener, err = net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	srv.registerAPI()
	srv.registerParent()

	go func() {
		err = srv.server.Serve(tcpKeepAliveListener{srv.listener.(*net.TCPListener)})
		if err != nil {
			log.Fatal(err)
		}
	}()
	fmt.Printf("Relay server running on '%s'\n", endpoint)

	return srv
}

// GetLocalEndpoint returns the local listener endpoint
func (srv *RelayServer) GetLocalEndpoint() string {
	return srv.listener.Addr().String()
}

func (srv *RelayServer) addSubscription(filename string, endpoint string) error {
	srv.Files.RLock()
	config, ok := srv.Files.Collection[filename]
	srv.Files.RUnlock()

	if !ok {
		return errors.New("file not found")
	}

	srv.SubscriptionsLock.Lock()
	defer srv.SubscriptionsLock.Unlock()

	srv.Subscriptions[endpoint] = Subscription{
		endpoint: endpoint,
		info:     &config,
	}

	return nil
}

func (srv *RelayServer) removeSubscription(filename string, endpoint string) error {
	srv.SubscriptionsLock.Lock()
	defer srv.SubscriptionsLock.Unlock()

	delete(srv.Subscriptions, endpoint)
	return nil
}

// Register HTTP API
func (srv *RelayServer) registerAPI() {
	srv.router.RedirectSlash = true
	srv.router.Post("/config/{filename}/subscribers/{endpoint}", http.HandlerFunc(srv.httpAddSubscription))
	srv.router.Delete("/config/{filename}/subscribers/{endpoint}", http.HandlerFunc(srv.httpRemoveSubscription))
}

func (srv *RelayServer) registerParent() {
	// just return if we dont have a parent
	if srv.parentEndpoint == "" {
		return
	}

	url := fmt.Sprintf("http://%s/config/testconf/subscribers/%s", srv.parentEndpoint, srv.endpoint)
	response, err := http.Post(url, "", nil)
	if err != nil {
		fmt.Printf("error subscribing to parent %s: %s\n", srv.parentEndpoint, err)
	} else {
		defer response.Body.Close()
		if response.StatusCode != http.StatusOK {
			fmt.Printf("error subscribing to parent %s: %d %s\n", srv.parentEndpoint, response.StatusCode, response.Status)
		}
	}
}

// HTTP handler
func (srv *RelayServer) httpAddSubscription(w http.ResponseWriter, r *http.Request) {
	filename := httpRouter.GetParam(r, "filename")
	endpoint := httpRouter.GetParam(r, "endpoint")
	srv.addSubscription(filename, endpoint)
}

func (srv *RelayServer) httpRemoveSubscription(w http.ResponseWriter, r *http.Request) {
	filename := httpRouter.GetParam(r, "filename")
	endpoint := httpRouter.GetParam(r, "endpoint")
	err := srv.removeSubscription(endpoint, filename)
	if err != nil {
		http.Error(w, err.Error(), 400)
	}
}
