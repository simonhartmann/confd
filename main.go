package main

// TODO / IDEAS:
// * retry missed config updates
// * batch SetConfigs (atomic)
// * version number for admin server actions?

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type Subscription struct {
	info     *FileInfo
	endpoint string
}

type FileInfo struct {
	name       string
	lastUpdate time.Time
	data       []byte
}

type FileContainer struct {
	sync.RWMutex
	Collection map[string]FileInfo
}

func CreateFileContainer() *FileContainer {
	return &FileContainer{
		Collection: make(map[string]FileInfo),
	}
}

type MyConfig struct {
	LocalAdminEndpoint string
	LocalRelayEndpoint string
	Parent             string
}

func readConfig() (cfg MyConfig) {
	cfgPath := *flag.String("cfg", "", "path to json config file")

	if cfgPath != "" {
		b, err := ioutil.ReadFile(cfgPath)
		if err != nil {
			log.Fatal(err)
		}

		err = json.Unmarshal(b, &cfg)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		cfg.LocalRelayEndpoint = *flag.String("node", ":4000", "")
		cfg.LocalAdminEndpoint = *flag.String("admin", ":4001", "")
		cfg.Parent = *flag.String("parent", "", "")
	}

	return cfg
}

func main() {
	fmt.Println("starting...")

	cfg := readConfig()
	container := FileContainer{
		Collection: make(map[string]FileInfo),
	}

	StartRelayServer(cfg.LocalRelayEndpoint, cfg.Parent, &container)
	StartAdminServer(cfg.LocalAdminEndpoint, &container)

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)

	s := <-ch
	fmt.Printf("received signal '%s'\n", s)

	fmt.Println("done")
}
